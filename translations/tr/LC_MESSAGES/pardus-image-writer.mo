��    %      D  5   l      @  ]   A     �  4   �     �  )   �                8     Y  #   e     �  
   �     �     �     �     �     �     �     �     
       D   0  ,   u     �     �     �  
   �  -   �          *  '   H     p     �     �     �     �  k  �  P   B  	   �  *   �     �  '   �  	   �  *   	     3	     M	  (   a	     �	  
   �	     �	  	   �	     �	     �	     �	     �	  +   �	     "
     @
  O   U
  5   �
     �
     �
     �
  	     3        R     n  /   �     �     �      �     �                 
                                                                                                       !                          %   	         #                "      $    <b>WARNING:</b>
All the personal data on the device will be deleted.
Do you want to continue? About An error occured while writing the file to the disk. Are you sure? Calculating the hash value of the file... Cancel Check ISO online (Pardus only) Checking the file's integrity... Checking... Could not connect to pardus.org.tr. DD Mode [Recommended] Emin Fedar Error! Exit ISO Mode Install Installation done. Installation failed. Integrity checking failed. Pardus Image Writer Please install {} Please make sure the USB device is connected properly and try again. Retrieving data from <b>pardus.org.tr</b>... Select ISO File... Start Target {} does not exists This file: This is not a Pardus ISO, or it is corrupted. TÜBİTAK ULAKBİM | ©2020 Will be written on this disk: Write ISO Images to USB devices easily. Write a new file. Writing process is finished. You can eject the USB disk. pardus.org.tr {} not found Project-Id-Version: 0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-07-01 15:56+0300
Last-Translator: Emin Fedar <emin.fedar@pardus.org.tr>
Language-Team: Turkish <gelistirici@pardus.org.tr>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0
X-Generator: Gtranslator 3.36.0
 <b>UYARI:</b>
Cihazdaki tüm verileriniz silinecek.
Devam etmek istiyor musunuz? Hakkında Dosya diske yazılırken bir hata oluştu. Emin misiniz? Dosyanın hash değeri hesaplanıyor... İptal Et ISO'yu çevrimiçi doğrula (Pardus için) Dosya kontrol ediliyor... Kontrol ediliyor... pardus.org.tr adresine bağlanılamadı. DD Modu [Önerilen] Emin Fedar Hata! Çıkış ISO Modu Yükle Yükleme tamamlandı. Yükleme başarısız. Veri bütünlüğü kontrolü başarısız. Pardus Disk Kalıbı Yazıcı Lütfen {} yükleyin Lütfen USB cihazın düzgün takılı olduğundan emin olun ve tekrar deneyin. Veriler <b>pardus.org.tr</b> adresinden alınıyor... ISO Dosyası seçiniz... Başlat {} hedefi mevcut değil Bu dosya: Bu bir Pardus Kalıbı değil veya dosya bozulmuş. TÜBİTAK ULAKBİM | ©2020 Bu diske yazılacak: ISO kalıplarını USB disklere kolayca yazın. Yeni dosya yaz. Yazma işlemi bitti. USB Belleği çıkarabilirsiniz. pardus.org.tr {} bulunamadı 